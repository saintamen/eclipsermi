import server.*;
import java.util.*;

public class OverbidStrategy implements Strategy {
	protected Map<String, OverbidThread> watchedBids = new HashMap<String, OverbidThread>();
	
	public void updateWithStrategy(Item item, Client client, IAuctionServer auctionServer) {
		if (!watchedBids.containsKey(item.getName())) {
			OverbidThread thread = new OverbidThread(item, auctionServer);
			thread.start();
			watchedBids.put(item.getName(), thread);
		}
	}
	
	//say who are you
	@Override
	public String toString() {
		return "OverbidStrategy";
	}
}