import server.*;

public interface Strategy {
	public void updateWithStrategy(Item item, Client client, IAuctionServer auctionServer);
}