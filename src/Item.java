import java.util.*;
import java.rmi.RemoteException;
import java.io.*;

public class Item implements Serializable {
	protected String ownerName;
	protected String name;
	protected String description;
	protected double currentBid;
	protected String currentBiddersName;
	protected long remainingTime;
	protected long startTime;
	private List<IAuctionListener> observers = new ArrayList<IAuctionListener>();
	
	public Item(String ownerName, String itemName, String itemDesc, double initialBid, int remainingTime) {
		this.ownerName = ownerName;
		name = itemName;
		currentBid = initialBid;
		currentBiddersName = null;
		this.remainingTime = (long)remainingTime;
		description = itemDesc;
		
		startTime = (new Date().getTime())/1000L;
	}
	
	public String getOwnerName() {
		return ownerName;
	}
	
	public String getName() {
		return name;
	}
	
	public double getCurrentBid() {
		return currentBid;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	//returns true if bid is successfuly done
	public boolean setCurrentBid(double bid, String biddersName) {
		long now = (new Date().getTime()/1000L);
		
		if (bid > currentBid && (startTime + remainingTime) > now) {
			currentBid = bid;
			currentBiddersName = biddersName;
			
			notifyAuctioners();
			return true;
		}
		//debug
		else {
			if (bid <= currentBid) {
				System.out.printf("Current bid: %f Proposed: %f\n", currentBid, bid);
			}
		}
		
		return false;
	}
	
	public String getCurrentBiddersName() {
		return currentBiddersName;
	}
	
	public long getRemainingTime() {
		long now = (new Date().getTime()/1000L);
		return (startTime + remainingTime) - now;
	}
	
	//for implementing observer pattern, notfiies all item observers
	public void notifyAuctioners() {
		for (IAuctionListener auctioner : observers) {
			try {
				auctioner.update(this);
			}
			catch (RemoteException e) {
				System.err.println("Unable to notify one of the auctioner!");
				e.printStackTrace();
			}
		}
	}
	
	public void addListener(IAuctionListener listener) {
		if (listener != null) {
			observers.add(listener);
		}
		else {
			System.out.println("Passed a null listener!");
		}
	}
	
}