import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IAuctionServer extends Remote {
    //<T> T executeTask(Task<T> t) throws RemoteException;
	public void placeItemForBid(String ownerName, String itemName, String itemDesc, double startBid, int auctionTime) throws RemoteException;
	public void bidOnItem(String bidderName, String itemName, double bid) throws RemoteException;
	public Item[] getItems() throws RemoteException;
	public void registerListener(IAuctionListener auctionListener, String itemName) throws RemoteException;
}
