import server.*;

class OverbidThread extends Thread {
	Item watchedItem;
	IAuctionServer auctionServer;
	
	public OverbidThread(Item item, IAuctionServer auctionServer) {
		watchedItem = item;
		this.auctionServer = auctionServer;
	}
	
	public void run() {
		try {
			Thread.sleep((watchedItem.getRemainingTime() - 2)*1000);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			double recentValue = 0.0;
			
			Item items[] = auctionServer.getItems();
			for (Item i : items) {
				if (i.getName().equals(watchedItem.getName())) {
					recentValue = i.getCurrentBid();
					break;
				}
			}
			
			if (recentValue > 0.0) {
				auctionServer.bidOnItem(Client.instance().getName(), watchedItem.getName(), recentValue * 2);
			}
		}
		catch (Exception e) {
			System.out.printf("I was unable to overbid twice the %s\n", watchedItem.getName());
		}
	}
}