import java.util.*;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IAuctionListener extends Remote {
    //T execute();
	
	//for observer pattern
	public void update(Item item) throws RemoteException;
}
